//Author - George Komen - 254713014492
#include <LiquidCrystal.h>
LiquidCrystal lcd(19, 18, 17, 16, 15, 14);
char imei[16];
//String today;
//String yesterday = "";
//String Lon;
//String Lat;
#include <SoftwareSerial.h>
SoftwareSerial gprsSerial(2, 3); //rx tx--suppose to be 3,2
#include <EEPROM.h>
int statuz = 0;
int addr2s = 1;
int addr3 = 202;
int housepower = 9;
int buzzer = 11;
int mosfetS = 5;
int mosfetB = 10;
int g_reset = 4;
int tamper = 12;
int tamperV;
unsigned long baudrate = 9600;
bool toggle1;
bool toggle2;
String content1;
String signal1;
int led = 9;
int button = 10;
int c4 = 8;
int c3 = 7;
int c2 = 6;
int c1 = 5;
unsigned long mch1 = 0;
unsigned long chint = 10000;
void(* resetFunc) (void) = 0;//declare reset function at address 0
bool serial = true;

void setup() {
  digitalWrite(g_reset, 1);
  if (serial) {
    Serial.begin(baudrate);
    Serial.println(F("rebooting"));
  }
  pinMode(housepower, INPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(g_reset, OUTPUT);
  pinMode(tamper, INPUT);
  pinMode(mosfetS, OUTPUT);
  pinMode(mosfetB, OUTPUT);
  pinMode(led, INPUT);
  pinMode(button, OUTPUT);
  pinMode(c1, INPUT);
  pinMode(c2, INPUT);
  pinMode(c3, INPUT);
  pinMode(c4, INPUT);
  delay(1000);
  mch1 = millis();
  setStatus(0);
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Booting");
  lcd.setCursor(0, 1);
  lcd.print("Controller V4");
  displayMSG();
  initializeSoftwareSerial();
  resetGsm();
  setupSMS();
  step4();
}

void loop() {
  unsigned long mch2 = millis();
  if (mch2 - mch1 > chint) {
    isCharging();
    mch1 = millis();
  }
  readSMS();
  displayMSG();
  //  resetProgramAfterADay();
}

void toggle() {
  digitalWrite(mosfetS, HIGH);
  digitalWrite(mosfetB, HIGH);
  delay(600);
  digitalWrite(mosfetS, LOW);
  digitalWrite(mosfetB, LOW);
  if (serial) {
    Serial.println(F("switched"));
  }
}
void ON() {
  int pw = digitalRead(housepower);
  if ( pw == 0 ) {
    if (serial) {
      Serial.println(F("............on toggle............"));
    }
    toggle();
  }
}

void OFF() {
  int pw = digitalRead(housepower);
  if (pw == 1 ) {
    if (serial) {
      Serial.println(F(".............off toggle..........."));
    }
    toggle();
  }
}

void resetGsm() {
  digitalWrite(g_reset, 0);
  delay(200);
  digitalWrite(g_reset, 1);
  delay(5000);
}

void step4() {
  delay(1000);
  gprsSerial.println("AT+CGSN\r\n");
  toSerial();
  if (content1.startsWith("8")) {

    String msg = content1.substring(0, 15);
    int copysmsl = msg.length() + 1;
    msg.toCharArray(imei, copysmsl);
    if (serial) {
      Serial.println(F("Got IMEI"));
      Serial.println(imei);
    }
  }
  else {
    if (serial) {
      Serial.println(F("Error getting imei number"));
    }
    delay(2000);
  }
}

void readSENSORS() {
  tamperV = digitalRead(tamper);
}

//void step7() {
//  gprsSerial.println("AT+CIPGSMLOC=1,1\r\n");
//  toSerial();
//  if (content1.startsWith("+CIPGSMLOC: 0,")) {
//    Lon = content1.substring(14, 23);
//    Lat = content1.substring(24, 33);
//    today = content1.substring(34, 44);
//    if (serial) {
//      Serial.println(F("Got Location")); //+CIPGSMLOC: 0,-4.974037,39.646976,2016/07/24,21:05:11OK
//      Serial.println(Lon + "  " + Lat + "  " + today);
//    }
//  }
//  else {
//    if (serial) {
//      Serial.print("ERROR: ");
//      Serial.println(content1);
//    }
//  }
//}

//void resetProgramAfterADay() {
//  if (yesterday == "") {
//    yesterday = today;
//  }
//  if (yesterday != today) {
//    resetFunc();
//    yesterday = today;
//  }
//}

void indicateConnection() {
  tone(buzzer, 300, 2000);
}

void wait() {
  unsigned long m1 = millis();
  unsigned long m2;
  if (serial) {
    Serial.println(F("waiting"));
  }
  while (gprsSerial.available() == 0) {
    m2 = millis();
    delay(1000);
    if ((m2 - m1) > 20000) {
      break;
    }
  }
}

void read1(unsigned long ttt) {
  if (gprsSerial.available() > 0) {
    char ci;
    content1 = "";
    while (gprsSerial.available() > 0) {
      ci = gprsSerial.read();
      if (ci == 0x0A || ci == 0x0D) {
      }
      else {
        delay(ttt);
        content1.concat(ci);
      }
    }
    if (content1.indexOf("RING") != -1) {
      receiveCall();
      step10();
    }
    if (content1.indexOf("+CMTI") != -1) {
      readSMS();
    }
    if (content1.indexOf("smsd") != -1) {
      setstep2(content1);
    }
    if (content1.indexOf("smsc") != -1) {
      step8();
    }
    
    if (content1.indexOf("smsr") != -1) {
      feedback();
    }
  }
}

void toSerial() {
  wait();
  read1(10);
  if (serial) {
    if (content1 != "") {
      Serial.println(content1);
    }
  }
}

void initializeSoftwareSerial() {
  gprsSerial.begin(baudrate);
  delay(2000);
  gprsSerial.write("ATE0\r\n");
  toSerial();
}

void setupSMS() {
  gprsSerial.println("AT+CMGF=1\r\n");  // set SMS mode to text
  toSerial();
  gprsSerial.println("AT+CNMI=1,1,0,0,0\r\n");
  toSerial();
  gprsSerial.println("AT+CPMS=\"SM\"\r\n"); //SM OR ME OR MT
  toSerial();
  gprsSerial.println("AT+CSAS\r\n");//save settings
  toSerial();
}

void sendSMS(char message[43]) {
  Serial.flush();
  gprsSerial.flush();
  
  String smsnumber2 = "AT+CMGS=\""+readSN()+"\"\r\n";
  int smsnumberl = smsnumber2.length() + 1;
  char copysmsnumber[smsnumberl];
  smsnumber2.toCharArray(copysmsnumber, smsnumberl);
  
  gprsSerial.println("AT+CMGF=1\r\n");
  toSerial();
  gprsSerial.write(copysmsnumber);
  toSerial();
  gprsSerial.write(message);
  delay(1000);
  gprsSerial.write((char)26);
  delay(1000);
  if (serial) {
    Serial.println(copysmsnumber);
    Serial.println(message);
  }
}

void readSMS() {
  for (int i = 0; i < 10; i++) {
    gprsSerial.println("AT+CMGR = " + String(i));
    toSerial();
  }
  deleteAllSMS();
}

void deleteAllSMS() {
  gprsSerial.println("AT+CMGD=1,4\r\n");
  toSerial();
}

void step8() {
  if (serial) {
    Serial.println(F("sms control"));
    Serial.println(content1);
  }
  //first control
  signal1 = content1.substring(content1.indexOf("$") + 1, content1.indexOf("%"));
  //second control
  int firstv1 = content1.indexOf("$") + 1;
  int firstv2 = content1.indexOf("%") + 1;
  writeSN(content1.substring(content1.indexOf("$", firstv1) + 1, content1.indexOf("%", firstv2)));
  setStatus(1);
}

void setStatus(int sender) {
  if (signal1 == "1") {
    EEPROM.write(statuz, 1);
    if (serial) {
      Serial.println(F("OFF"));
    }
    toggle1 = false;
  }
  else if (signal1 == "0") {
    EEPROM.write(statuz, 0);
    if (serial) {
      Serial.println(F("ON"));
    }
    toggle1 = true;
  }
  if (toggle1 !=  toggle2) {
    toggle2 = toggle1;
    indicateConnection();
  }
  int eeph = EEPROM.read(statuz);
  if (eeph == 1) {
    ON();
    if (serial) {
      Serial.println(F("system ON"));
    }
  }
  else if (eeph == 0) {
    OFF();
    if (serial) {
      Serial.println(F("system OFF"));
    }
  }
  if (sender == 1) {
    step11();
  }
}

void dtmfSetup() {
  gprsSerial.println("AT+DDET=1\r\n");//dtmf
  toSerial();
}

void receiveCall() {
  dtmfSetup();
  gprsSerial.println("ATA\r\n");//receive call
  toSerial();
}

void step10() {
  unsigned long m1 = millis();
  unsigned long m2;
  while (1) {
    waitOnly();
    char ci;
    String content2;
    while (gprsSerial.available()) {
      ci = gprsSerial.read();
      if (ci == 0x0A || ci == 0x0D) {
      }
      else {
        content2.concat(ci);
        delay(50);
      }
    }
    String val  = content2.substring(7, 8);
    if (serial) {
      Serial.println(val);
    }
    if (content2.indexOf("CARRIER") >= 0) {
      break;
    }
    if (val == "0") {
      //generate dtmf
      gprsSerial.println("AT+VTS=0\r\n");//receive call
      toSerialOnly();
      content1 = "$0%$" + readSN() + "%";
      step8();
    }
    if (val == "1") {
      //generate dtmf
      gprsSerial.println("AT+VTS=1\r\n");//receive call
      toSerialOnly();
      content1 = "$1%$" + readSN() + "%";
      step8();
    }
    if (val == "#") {
      //generate dtmf
      gprsSerial.println("AT+VTS=#\r\n");//receive call
      toSerialOnly();
      resetFunc();
      delay(2000);
      //resetProgram();
    }
    if (val == "*") {
      //generate dtmf
      gprsSerial.println("AT+VTS=*\r\n");//receive call
      toSerialOnly();
      diagnose();
    }
    m2 = millis();
    if ((m2 - m1) > 20000) {
      gprsSerial.println("ATH\r\n");//receive call
      toSerial();
      break;
    }
  }
}

void diagnose() {
  //diagnose
  char status1[1];
  int n = EEPROM.read(statuz);
  String(n).toCharArray(status1, 1);
  if (serial) {
    Serial.println("status: " + String(n));
  }

  char c1[43];
  c1[0] = '\0';
  strcat(c1, "imei: ");
  strcat(c1, imei);
  strcat(c1, " status?: ");
  strcat(c1, status1);
  strcat(c1, "\r\n");

  sendSMS(c1);
}

void toSerialOnly() {
  waitOnly();
  if (serial) {
    Serial.println(readOnly());
  }
}

String readOnly() {
  char ci;
  String content2;
  while (gprsSerial.available() > 0)
  {
    ci = gprsSerial.read();
    if (ci == 0x0A || ci == 0x0D) {
    }
    else {
      content2.concat(ci);
      delay(5);
    }
  }
  return content2;
}

void waitOnly() {
  unsigned long m1 = millis();
  unsigned long m2;
  if (serial) {
    Serial.println(F("waiting only"));
  }
  while (gprsSerial.available() == 0) {
    m2 = millis();
    delay(50);
    if ((m2 - m1) > 2000) {
      break;
    }
  }
}

void setstep2(String msg) {
  writeE(msg.substring(msg.indexOf("smsd"), msg.length()));
  displayMSG();
}

void displayMSG() {
  String message = readE();
  if (message.indexOf("smsd") == -1) {
    message = "smsd$Sunami%s*Not linked to the server#e";
    writeE(message);
  }
  lcd.setCursor(0, 1);
  lcd.print(message.substring(message.indexOf("s*") + 2, message.indexOf("#e")));
  delay(700);

  int mbl = message.substring(message.indexOf("s*") + 2, message.indexOf("#e")).length();
  for (int i = 0; i < mbl; i += 1) {
    if (mbl - i <= 16) {
      lcd.setCursor(0, 0);
      lcd.print(message.substring(message.indexOf("d$") + 2, message.indexOf("%s")));
      lcd.setCursor(0, 1);
      lcd.print(message.substring(message.indexOf("s*") + 2, message.indexOf("#e")).substring(i, i + 16));
      delay(500);
      break;
    }
    lcd.setCursor(0, 1);
    lcd.print(message.substring(message.indexOf("s*") + 2, message.indexOf("#e")).substring(i, i + 16));
    delay(400);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(message.substring(message.indexOf("d$") + 2, message.indexOf("%s")));
  }
}

//message should not exxceed 200 chars
void writeE(String a) {
  for (int i = 0; i < 201; i++) {
    char c = a.charAt(i);
    EEPROM.write(addr2s, c);
    addr2s++;
  }
  addr2s = 1;
}

String readE() {
  String n1;
  for (int i = 1; i < 201; i++) {
    char value = EEPROM.read(addr2s);
    n1.concat(value);
    addr2s++;
  }
  addr2s = 1;
  return n1;
}

//led, button, c1, c2, c3, c4
void isCharging() {
  unsigned long m1 = millis();
  unsigned long m2 = millis();
  unsigned long checktime = 3000;
  int total = 0;
  int charges[3];
  while (m2 - m1 < checktime) {
    int rc1 = digitalRead(c1);
    if (rc1 == 1) {
      charges[0] = 1;
      lcd.setCursor(12, 0);
      lcd.print("25%)");
    }
    else {
      charges[0] = 0;
    }
    int rc2 = digitalRead(c2);
    if (rc2 == 1) {
      charges[1] = 1;
      lcd.setCursor(12, 0);
      lcd.print("50%)");
    }
    else {
      charges[1] = 0;
    }
    int rc3 = digitalRead(c3);
    if (rc3 == 1) {
      charges[2] = 1;
      lcd.setCursor(12, 0);
      lcd.print("75%)");
    }
    else {
      charges[3] = 0;
    }
    int rc4 = digitalRead(c4);
    if (rc4 == 1) {
      charges[3] = 1;
      lcd.setCursor(12, 0);
      lcd.print("100)");
    }
    else {
      charges[3] = 0;
    }
    m2 = millis();
  }
  int ret = charges[0] + charges[1] + charges[2] + charges[3];
  if (ret > 1) {
    // "charging";
    lcd.setCursor(9, 0);
    lcd.print("(C ");
  }
  else {
    // "not charging";
    lcd.setCursor(9, 0);
    lcd.print("(NC");
  }
}

void step11() {
  int pw = digitalRead(housepower);
  if (pw == 1 ) {

    char c[43];
    c[0] = '\0';
    strcat(c, "*1sunami#1 *2");
    strcat(c, imei);
    strcat(c, "#2 *31#3\r\n");

    sendSMS(c);
  } else {

    char c1[43];
    c1[0] = '\0';
    strcat(c1, "*1sunami#1 *2");
    strcat(c1, imei);
    strcat(c1, "#2 *30#3\r\n");

    sendSMS(c1);
  }
}

void writeSN(String a) {
  for (int i = 0; i < 13; i++) {
    char c = a.charAt(i);
    EEPROM.write(addr3, c);
    addr3++;
  }
  addr3 = 202;
}

String readSN() {
  String n1;
  for (int i = 0; i < 13; i++) {
    char value = EEPROM.read(addr3);
    n1.concat(value);
    addr3++;
  }
  addr3 = 202;
  return n1;
}

void feedback(){
    int pw = digitalRead(housepower);
  if (pw == 1 ) {

    char c[43];
    c[0] = '\0';
    strcat(c, "*1sunami#1 *2");
    strcat(c, imei);
    strcat(c, "#2 *31(Q)#3\r\n");

    sendSMS(c);
  } else {

    char c1[43];
    c1[0] = '\0';
    strcat(c1, "*1sunami#1 *2");
    strcat(c1, imei);
    strcat(c1, "#2 *30(Q)#3\r\n");

    sendSMS(c1);
  }
}

